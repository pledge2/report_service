package bootstrap

import (
	"report_service/internal/config"
	"report_service/internal/drivers/services/assessment_service"
	"report_service/internal/pkg/logger"
)

type Clients struct {
	assessment assessment_service.Client
}

func initClients(cfg config.Config, log logger.Logger) Clients {
	assessmentClient := assessment_service.New(cfg, log)

	return Clients{
		assessment: *assessmentClient,
	}
}
