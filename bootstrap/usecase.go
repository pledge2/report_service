package bootstrap

import (
	"report_service/internal/pkg/logger"
	pdf_usecase "report_service/internal/usecase/report"
)

type useCases struct {
	pdfUC pdf_usecase.UseCase
}

func initUsecases(client Clients, log logger.Logger) useCases {
	pdfUseCase := pdf_usecase.New(&client.assessment, log)
	return useCases{
		pdfUC: *pdfUseCase,
	}
}
