package pdf_usecase

import (
	"context"
	"report_service/internal/domain"
	"report_service/internal/pkg/logger"
)

type assessmentI interface {
	AssessmentReport(ctx context.Context, id string) (domain.PdfData, error)
}

type UseCase struct {
	assessment assessmentI
	log        logger.Logger
}

func New(a assessmentI, log logger.Logger) *UseCase {
	return &UseCase{
		assessment: a,
		log:        log,
	}
}

func (uc *UseCase) GetReportData(ctx context.Context, assessmentID string) (domain.PdfData, error) {
	var (
		logMsg = "uc.Pdf.GetReportData "
	)
	result, err := uc.assessment.AssessmentReport(ctx, assessmentID)
	if err != nil {
		uc.log.Error(logMsg+"uc.assessment.AssessmentReport failed", logger.Error(err))

		return domain.PdfData{}, err
	}

	return result, nil
}
