package domain

import "time"

type PdfData struct {
	CardID               string
	EmpBranchID          string
	EmpName              string
	EmpCBID              int
	OwnerFullName        string
	OwnerPinfl           string
	TransportName        string
	TransportVIN         string
	TransportPlateNum    string
	TransportYear        int
	TransportMileage     int
	TransportCondition   string
	TransportFuel        string
	TransportMarketPrice int
	TransportCreditPrice int
	TransportComment     string
	AnalogCount          string
	UpdatedAt            time.Time
	AnalogData           []AnalogData
}

type AnalogData struct {
	AnalogID          string
	AnalogName        string
	AnalogYear        int
	AnalogMileage     int
	AnalogMarketPrice int
	AnalogUrl         string
}
