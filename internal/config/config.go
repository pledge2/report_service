package config

type Config struct {
	Environment string `env:"ENVIRONMENT"`
	LogLevel    string `env:"LOG_LEVEL"`
	ServerIP    string `env:"SERVER_IP"`
	HTTPPort    string `env:"HTTP_PORT"`
	ServiceName string `env:"SERVICE_NAME"`

	AssessmentService string `env:"ASSESSMENT_SERVICE"`
}
