package http

import (
	"encoding/json"
	"errors"
)

type DBOResponse struct {
	Status    string          `json:"status"`
	ErrorCode int             `json:"error_code"`
	ErrorNote string          `json:"error_note"`
	Data      json.RawMessage `json:"data"`
}

func (d DBOResponse) Scan(model interface{}) error {
	err := json.Unmarshal(d.Data, &model)
	if err != nil {
		return e(ErrUnexpectedResponseData, err.Error(), false)
	}
	return nil
}

func (d DBOResponse) ToError() error {
	if d.ErrorCode != 0 {
		return errors.New(d.ErrorNote)
	}
	return nil
}
