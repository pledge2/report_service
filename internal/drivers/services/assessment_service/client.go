package assessment_service

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"report_service/internal/config"
	"report_service/internal/domain"
	"report_service/internal/errs"
	service "report_service/internal/pkg/http"
	"report_service/internal/pkg/logger"
	"report_service/internal/pkg/status"
)

type Client struct {
	client *http.Client
	cfg    config.Config
	log    logger.Logger
}

func New(cfg config.Config, log logger.Logger) *Client {
	return &Client{
		client: &http.Client{},
		cfg:    cfg,
		log:    log,
	}
}

func (c *Client) AssessmentReport(ctx context.Context, id string) (domain.PdfData, error) {
	var (
		logMsg   = "service.Assessment.AssessmentReport "
		url      = c.cfg.AssessmentService + assessmentGetReport
		result   domain.PdfData
		response service.DBOResponse
		data     ReportData
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+" http.NewRequestWithContext failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	q := request.URL.Query()
	q.Add("assessmentID", id)
	request.URL.RawQuery = q.Encode()

	resp, err := c.client.Get(request.URL.String())
	if err != nil {
		c.log.Error(logMsg+"c.Client.Get failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal failed", logger.Error(err))
		c.log.Info(fmt.Sprint(logMsg+"Response", string(res)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(fmt.Sprint(logMsg+"Response", string(res), response.ErrorNote))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+" result.Scan failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.PdfData{
		CardID:               data.CardID,
		EmpBranchID:          data.EmpBranchID,
		EmpName:              data.EmpName,
		EmpCBID:              data.EmpCBID,
		OwnerFullName:        data.OwnerFullName,
		OwnerPinfl:           data.OwnerPinfl,
		TransportName:        data.TransportName,
		TransportVIN:         data.TransportVIN,
		TransportPlateNum:    data.TransportPlateNum,
		TransportYear:        data.TransportYear,
		TransportMileage:     data.TransportMileage,
		TransportCondition:   data.TransportCondition,
		TransportFuel:        data.TransportFuel,
		TransportMarketPrice: data.TransportMarketPrice,
		TransportCreditPrice: data.TransportCreditPrice,
		TransportComment:     "Noma'lum",
		AnalogCount:          data.AnalogCount,
		UpdatedAt:            data.AssessmentUpdatedTime,
	}
	for _, val := range data.AnalogData {
		aData := domain.AnalogData{
			AnalogID:          val.ID,
			AnalogName:        val.Name,
			AnalogYear:        val.Year,
			AnalogMileage:     val.Speedometer,
			AnalogMarketPrice: val.Price,
			AnalogUrl:         val.AnalogUrl,
		}
		result.AnalogData = append(result.AnalogData, aData)
	}
	return result, err
}
