package assessment_service

import "time"

const assessmentGetReport = "/assessment/get/report"

type ReportData struct {
	CardID                string    `json:"card_id"`
	EmpBranchID           string    `json:"emp_branch_id"`
	EmpName               string    `json:"emp_name"`
	EmpCBID               int       `json:"emp_cbid"`
	OwnerFullName         string    `json:"owner_full_name"`
	OwnerPinfl            string    `json:"owner_pinfl"`
	TransportName         string    `json:"transport_name"`
	TransportVIN          string    `json:"transport_vin"`
	TransportPlateNum     string    `json:"transport_plate_num"`
	TransportYear         int       `json:"transport_year"`
	TransportMileage      int       `json:"transport_mileage"`
	TransportCondition    string    `json:"transport_condition"`
	TransportFuel         string    `json:"transport_fuel"`
	TransportMarketPrice  int       `json:"transport_market_price"`
	TransportCreditPrice  int       `json:"transport_credit_price"`
	AssessmentUpdatedTime time.Time `json:"assessment_updated_time"`
	AnalogCount           string    `json:"analog_count"`
	AnalogData            []Analog  `json:"analog_data"`
}

type Analog struct {
	ID          string `json:"a_id"`
	Name        string `json:"name"`
	Year        int    `json:"year"`
	Speedometer int    `json:"speedometer"`
	Price       int    `json:"price"`
	AnalogUrl   string `json:"analog_url"`
}
