package rest

import (
	"bytes"
	"context"
	"fmt"
	"net/http"
	"os"
	"report_service/internal/errs"
	"strconv"
	"text/template"
	"time"

	"github.com/SebastiaanKlippert/go-wkhtmltopdf"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.hamkorbank.uz/libs/logger"
	"go.uber.org/zap"
)

const (
	templatePath = "internal/templates/index.html"
	storagePath  = "internal/storage/"
)

type pdfData struct {
	CardID                      string       `json:"card_id"`
	Filial                      string       `json:"filial"`
	Date                        string       `json:"assessment_created_time"`
	LoanInspector               string       `json:"loan_inspector"`
	InspectorCBID               int          `json:"inspector_cbid"`
	PropertyOwner               string       `json:"property_owner"`
	OwnerPinfl                  string       `json:"owner_pinfl"`
	TransportName               string       `json:"model_name"`
	TransportVinNum             string       `json:"vin_num"`
	TransportCarNumber          string       `json:"car_number"`
	TransportManufactureYear    int          `json:"manufacture_year"`
	TransportMileage            string       `json:"mileage"`
	TransportTechnicalCondition string       `json:"technical_condition"`
	TransportFuel               string       `json:"fuel"`
	TransportMarketPrice        string       `json:"market_price"`
	TransportCreditPrice        string       `json:"credit_price"`
	PropertyComment             string       `json:"property_comment"`
	AnalogCount                 string       `json:"analog_count"`
	AnalogData                  []analogData `json:"analog_data"`
}

type analogData struct {
	AnalogName        string `json:"analog_name"`
	AnalogYear        int    `json:"analog_year"`
	AnalogMileage     string `json:"analog_mileage"`
	AnalogMarketPrice string `json:"market_price"`
	AnalogID          string `json:"analogID"`
	AnalogUrl         string `json:"analog_url"`
}

// generatePDF godoc
// @Router /generate/pdf [POST]
// @Summary Create a new PDF Report for Assessment
// @Description API To Create a new PDF Report for Assessment
// @Tags Assessment PDF Report
// @Produce json
// @Param assessmentID query string true "AssessmentID -> id"
// @Success 200 {object} Response{data}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) generatePDF() gin.HandlerFunc {
	return func(c *gin.Context) {

		assessmentID := c.Query("assessmentID")

		if assessmentID == "" {
			Return(c, nil, errs.ErrAssessmentIDEmpty)
			return
		}

		if !IsValidUUID(assessmentID) {
			Return(c, nil, errs.ErrAssessmentIDFormat)
			return
		}

		reportData, err := s.pdfUc.GetReportData(c, assessmentID)
		if err != nil {
			Return(c, nil, err)
			return
		}

		req := pdfData{
			CardID:                      reportData.CardID,
			Filial:                      reportData.EmpBranchID,
			Date:                        reportData.UpdatedAt.Format("02.01.2006"),
			LoanInspector:               reportData.EmpName,
			InspectorCBID:               reportData.EmpCBID,
			PropertyOwner:               reportData.OwnerFullName,
			OwnerPinfl:                  reportData.OwnerPinfl,
			TransportName:               reportData.TransportName,
			TransportVinNum:             reportData.TransportVIN,
			TransportCarNumber:          reportData.TransportPlateNum,
			TransportManufactureYear:    reportData.TransportYear,
			TransportMileage:            formatNumber(reportData.TransportMileage),
			TransportTechnicalCondition: reportData.TransportCondition,
			TransportFuel:               reportData.TransportFuel,
			TransportMarketPrice:        formatNumber(reportData.TransportMarketPrice),
			TransportCreditPrice:        formatNumber(reportData.TransportCreditPrice),
			AnalogCount:                 reportData.AnalogCount,
			PropertyComment:             reportData.TransportComment,
		}

		for _, val := range reportData.AnalogData {
			data := analogData{
				AnalogName:        val.AnalogName,
				AnalogYear:        val.AnalogYear,
				AnalogMileage:     formatNumber(val.AnalogMileage),
				AnalogMarketPrice: formatNumber(val.AnalogMarketPrice),
				AnalogID:          val.AnalogID,
				AnalogUrl:         val.AnalogUrl,
			}
			req.AnalogData = append(req.AnalogData, data)
		}

		pdf, err := newPDF(c, req)
		if err != nil {
			Return(c, nil, err)
			return
		}

		currentDateTime := time.Now().Format("2006-01-02_15-04")
		filename := fmt.Sprintf("%s.pdf", currentDateTime)

		c.Writer.Header().Set("Content-Disposition", "attachment; filename="+filename)
		c.Writer.Header().Set("Content-Type", "application/pdf")
		c.Data(http.StatusOK, "application/pdf", pdf)

		Return(c, pdf, err)
	}
}

// generatePDF godoc
// @Router /generate/pdfs [GET]
// @Summary Create a new PDF Report for Assessment
// @Description API To Create a new PDF Report for Assessment
// @Tags Assessment PDF Report
// @Produce json
// @Success 200 {object} Response{data}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) generatePDFS() gin.HandlerFunc {
	return func(c *gin.Context) {
		ok := "Hello World"
		Return(c, ok, nil)
	}
}

func formatNumber(num int) string {
	var result string
	numStr := fmt.Sprintf("%d", num)

	for i := len(numStr) - 1; i >= 0; i-- {
		if (len(numStr)-i-1)%3 == 0 && i != len(numStr)-1 {
			result = " " + result
		}
		result = string(numStr[i]) + result
	}

	return result
}

func newPDF(ctx context.Context, data pdfData) ([]byte, error) {
	fileName := uuid.NewString()
	path := fmt.Sprint(storagePath, fileName, ".pdf")

	// Parse the template and populate with the provided data
	str, err := parseTemplate(ctx, templatePath, data)
	if err != nil {
		return nil, err
	}

	// Generate PDF with custom arguments
	args := []string{"no-pdf-compression"}

	// Generate PDF and return the file contents
	_, err = generatePDF(ctx, path, args, str)
	if err != nil {
		return nil, err
	}

	fileContent, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}

	// Remove the temporary PDF file
	err = os.Remove(path)
	if err != nil {
		return nil, err
	}

	return fileContent, nil
}

// parsing template function
func parseTemplate(ctx context.Context, templateFileName string, data interface{}) (string, error) {
	var l = logger.FromCtx(ctx, "pdf.ParseTemplate")

	t, err := template.ParseFiles(templateFileName)
	if err != nil {
		l.Error("uc.ParseFiles --", zap.Error(err))

		return "", err
	}

	buf := new(bytes.Buffer)
	if err = t.Execute(buf, data); err != nil {
		l.Error("uc.Execute --", zap.Error(err))

		return "", err
	}

	return buf.String(), nil
}

func generatePDF(ctx context.Context, pdfPath string, args []string, bdStr string) (bool, error) {
	var l = logger.FromCtx(ctx, "pdf.GeneratePDF")

	t := time.Now().Unix()

	if _, err := os.Stat("cloneTemplate/"); os.IsNotExist(err) {
		if errDir := os.Mkdir("cloneTemplate/", 0777); errDir != nil {
			l.Error("uc.os.Mkdir --", zap.Error(err))
		}
	}

	err1 := os.WriteFile("cloneTemplate/"+strconv.FormatInt(int64(t), 10)+".html", []byte(bdStr), 0644)
	if err1 != nil {
		l.Error("uc.os.WriteFile --", zap.Error(err1))
	}

	f, err := os.Open("cloneTemplate/" + strconv.FormatInt(int64(t), 10) + ".html")
	if f != nil {
		l.Error("uc.os.Open --", zap.Error(err))

		defer f.Close()
	}

	if err != nil {
		l.Error("uc.Open --", zap.Error(err))
	}

	pdfg, err := wkhtmltopdf.NewPDFGenerator()
	if err != nil {
		l.Error("uc.wkhtmltopdf.NewPDFGenerator --", zap.Error(err))
	}

	for _, arg := range args {
		switch arg {
		case "low-quality":
			pdfg.LowQuality.Set(true)
		case "no-pdf-compression":
			pdfg.NoPdfCompression.Set(true)
		case "grayscale":
			pdfg.Grayscale.Set(true)
		}
		//default
	}

	pdfg.AddPage(wkhtmltopdf.NewPageReader(f))
	pdfg.Cover.EnableLocalFileAccess.Set(true)
	pdfg.PageSize.Set(wkhtmltopdf.PageSizeA4)
	pdfg.Dpi.Set(300)

	err = pdfg.Create()
	if err != nil {
		l.Error("uc.pdfg.Create --", zap.Error(err)) //return
	}

	err = pdfg.WriteFile(pdfPath)
	if err != nil {
		l.Error("uc.pdfg.WriteFile --", zap.Error(err))
	}

	dir, err := os.Getwd()
	if err != nil {
		l.Error("uc.os.Getwd --", zap.Error(err))
	}

	defer os.RemoveAll(dir + "/cloneTemplate")

	return true, nil
}
