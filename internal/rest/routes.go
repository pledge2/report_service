package rest

import (
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func (s *Server) Router() {

	pdf := s.router.Group("generate")

	pdf.POST("/pdf", s.generatePDF())
	pdf.GET("/pdfs", s.generatePDFS())

	s.router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

}
