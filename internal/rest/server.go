package rest

import (
	"context"
	"net/http"
	"report_service/internal/domain"

	"gitlab.hamkorbank.uz/libs/logger"

	"github.com/gin-gonic/gin"
)

type pdfUseCase interface {
	GetReportData(ctx context.Context, assessmentID string) (domain.PdfData, error)
}

type Server struct {
	router *gin.Engine
	log    logger.Logger
	pdfUc  pdfUseCase
}

func New(router *gin.Engine, log logger.Logger, pdfUc pdfUseCase) *Server {
	srv := &Server{
		router: router,
		log:    log,
		pdfUc:  pdfUc,
	}
	srv.Router()
	return srv
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.router.ServeHTTP(w, r)
}
