FROM hub-nx.hamkor.local/devops/golang:1.20 as builder
WORKDIR /app
COPY . ./

RUN export GOPROXY=http://gitlab.hamkorbank.uz:3002 && \
    export CGO_ENABLED=0 && \
    export GOOS=linux && \
    make build && \
    mv ./bin/report /

# Install wkhtmltopdf
FROM hub-nx.hamkor.local/devops/alpine-wkhtmltopdf:3.16.2-0.12.6-full as wkhtmltopdf

# Final stage
FROM hub-nx.hamkor.local/devops/alpine:3.18.2
WORKDIR /app

RUN apk update && \
        apk add --no-cache \
        libstdc++ \
        libx11 \
        libxrender \
        libxext \
        libssl1.1 \
        ca-certificates \
        fontconfig \
        freetype \
        ttf-droid \
        ttf-freefont \
        ttf-liberation

COPY --from=wkhtmltopdf /bin/wkhtmltopdf /bin/libwkhtmltox.so /bin/
COPY --from=builder /app/internal/templates/ /app/internal/templates/
COPY --from=builder /app/internal/storage/ /app/internal/storage/
COPY --from=builder report .

CMD ["./report"]

