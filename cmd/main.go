package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"report_service/api/docs"
	"report_service/bootstrap"
	"report_service/internal/config"
	"report_service/internal/pkg/logger"

	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"github.com/sethvargo/go-envconfig"
)

func main() {
	quitSignal := make(chan os.Signal, 1)
	signal.Notify(quitSignal, os.Interrupt)
	_ = godotenv.Load()

	var cfg config.Config
	if err := envconfig.ProcessWith(context.TODO(), &cfg, envconfig.OsLookuper()); err != nil {
		panic(fmt.Sprintf("envconfig.Process: %s", err))
	}

	docs.SwaggerInfo.Host = cfg.ServerIP + cfg.HTTPPort
	docs.SwaggerInfo.Description = "Generating PDF Report"
	docs.SwaggerInfo.Schemes = []string{"http"}

	log := logger.New(cfg.LogLevel, "Report Service")
	log.Debug(fmt.Sprintf("config - %+v", cfg))

	ctx, cancel := context.WithCancel(context.Background())

	app := bootstrap.New(cfg, log, ctx)

	go func() {
		OSCall := <-quitSignal
		log.Info(fmt.Sprintf("system call:%+v", OSCall))
		cancel()
	}()

	app.Run(ctx)

	log.Info("REST Server Gracefully Shut Down")
}
